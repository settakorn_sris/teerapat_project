﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{ 
public class playerController : MonoBehaviour
{
    public float speed = 10.0f;
    public SpriteRenderer playerSprite;
    public GameObject swordPrefab;
    public Transform throwPoint;
    public static  bool isLookBack = false;
    public Rigidbody2D playerRigidbody;
    private bool massHigh;
    private float timeCounter;
    private bool isWeaponReady;
    public SpriteRenderer spritePlayer;

    void Start()
    {
        spritePlayer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
        PlayerController();
        WeaponThrow();
        TimeCount();
        
    }

    void PlayerController()
    {
        float translation = Input.GetAxis("Horizontal") * speed;
        translation *= Time.deltaTime;
        transform.Translate(translation, 0, 0);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            playerRigidbody.AddForce(transform.up * 300);

        }

        if (translation > 0)
        {
            playerSprite.flipX = false;
            isLookBack = false;

        }
        else if (translation < 0)
        {
            playerSprite.flipX = true;
            isLookBack = true;
        }
        
    }

    void WeaponThrow()
    {
        if (isWeaponReady == true)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Instantiate(swordPrefab, throwPoint.position, Quaternion.identity);
                isWeaponReady = false;
                timeCounter = 0;
            }
        }

    }
    

    void TimeCount()
    {
        timeCounter += Time.deltaTime;
        if (timeCounter > 1.2f)
        {
            isWeaponReady = true;
        }
    }
    
    
    
}
}
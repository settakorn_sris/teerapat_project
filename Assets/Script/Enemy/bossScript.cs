﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;
using UnityEngine.SceneManagement;



public class bossScript : MonoBehaviour
{
    // Start is called before the first frame update
    private float bossHP = 8;
    private float timeCounter;
    public GameObject axePrefab;
    public Transform throwPoint;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (bossHP == 0)
        {
            SceneManager.LoadScene("Winning");

        }
        
        ThrowAxe();
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            ScoreManager.ScoreValue = 0;
            SceneManager.LoadScene("GameOver");
        }
        
        if (other.tag == "Weapon")
        {
            Destroy(other.gameObject);
            bossHP--;
        }
    }

    void ThrowAxe()
    {
        timeCounter += Time.deltaTime;
        if (timeCounter > 3)
        {
            Instantiate(axePrefab, throwPoint.position, Quaternion.identity);
            timeCounter = 0;

        }
    }
    
    
}

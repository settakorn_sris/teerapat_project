﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class axeScript : MonoBehaviour
{
    private float timeCounter = 0;
    private float rangeAxe;

    void Start()
    {
       AxeForce();
    }

    // Update is called once per frame
    void Update()
    {
        AxeDestroy();
    }

    void AxeForce()
    {
        Rigidbody2D axeRigidbody;
        axeRigidbody = GetComponent<Rigidbody2D>();
        randomAxeRange();
        axeRigidbody.AddTorque(100,0);
        axeRigidbody.AddForce(transform.up * 135);
        axeRigidbody.AddForce(transform.right * rangeAxe);

    }

    void AxeDestroy()
    {
        timeCounter += Time.deltaTime;
        if (timeCounter > 2)
        {
            Destroy(this.gameObject);
            
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            SceneManager.LoadScene("GameOver");
        }

        if (other.tag == "Platform")
        {
            AxeDestroy();
        }
        
    }
    
    void randomAxeRange()
    {
       rangeAxe = UnityEngine.Random.Range(1,4);
       switch (rangeAxe)
       {
           case 1 : rangeAxe = -250;
               break;
           case 2 : rangeAxe = -350;
               break;
           case 3 : rangeAxe = -150;
               break;
       }
    }
    
    
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manager;
using UnityEngine.SceneManagement;

namespace Enemy
{ 
public class enemyScript : MonoBehaviour
{
        [SerializeField] AudioSource audioSource;
        [SerializeField] Transform player;
        [SerializeField] float argoRang;
        [SerializeField] float movespeed;
        private ScoreManager scoreManager;

        Rigidbody2D rb2d;
        private float timeCounter;
   
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float distToPlayer = Vector2.Distance(transform.position, player.position);

        if(distToPlayer<argoRang)
        {
            chasePlayer();
        }
        else
        {
            StopChasingPlayer();
        }
    }

    private void StopChasingPlayer()
    {
        rb2d.velocity = new Vector2(0, 0);
    }

    void chasePlayer()
    {
        if(transform.position.x<player.position.x)
        {
            rb2d.velocity = new Vector2(movespeed, 0);
        }
        else if(transform.position.x > player.position.x)
        {
            rb2d.velocity = new Vector2(-movespeed, 0);
            transform.localScale = new Vector2(-10, 10);
        }

    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            ScoreManager.ScoreValue = 0;
            SceneManager.LoadScene("GameOver");
            
        }
        
        if (other.tag == "Weapon")
        {
            Destroy(other);
            Destroy(this.gameObject);
            OnEnemyDead();
        }
        
    }
     private void OnEnemyDead()
        {
            ScoreManager.ScoreValue +=10;           

        }



}
}